const express = require("express");
const mongoose = require("mongoose");
// Allows our back-end application to be available to our front-end application
// Allows us to control app's Cross Origin Resource Sharing Settings
const cors = require("cors");
const app = express();

//Connect to our MongoDB database
mongoose.connect("mongodb+srv://admin:admin@zuitt-bootcamp.z9use.mongodb.net/Batch127_Booking?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

// Prompts a message in the terminal once the connection is "open" and we are able to successfully connect to our database
mongoose.connection.once("open", () => console.log("Now connected to MongoDB Atlas"));

// Allows all resources/origins to access our backend application
// Checks kung sino ang allowed origins ang allowed sa server
	//  put URL if may irerestrict
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.listen(process.env.PORT || 4000, () =>{
	console.log(`API is now online on port ${process.env.PORT || 4000}`)
})
// Will used the defined port number for the application whenever an environment variable is available 
	// OR will used port 4000 if none is defined
		// Since we are local, 4000 ang gagamitin
// This syntax will allow flexibility when using the application locally or as a hosted application
// Allows the port to be flexible locally or hosted application